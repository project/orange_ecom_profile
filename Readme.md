# Orange Ecom Profile

An installation profile for Drupal that includes common modules used in e-commerce sites.

## Contents

* [Introduction](#introduction).
* [Requirements](#requirements).
* [Installation](#installation).
* [Configuration](#configuration).
* [Maintainers](#maintainers).

## Introduction

This profile is part of the Orange Suite developed by [Acro Media Inc.](https://www.acromedia.com/).

This profile includes many common modules used while working with Drupal and Drupal Commerce.
There is also default configuration that will give you a head start on setting up your sites.

* For a full description of this profile, visit the [project page](https://www.drupal.org/project/orange_ecom_profile).
* To submit bug reports, give feature suggestions, or track changes visit the [issue queue](https://www.drupal.org/project/issues/orange_ecom_profile).

## Requirements

This profile has no special requirements outside of Composer requirements.

## Installation

Install as you would install any other [contributed Drupal profile](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules) using [Composer](https://getcomposer.org) :

1. Run `composer require drupal/orange_ecom_profile` (assuming you have set up the Drupal packages repository).

2. Select this profile when prompted in the Drupal installation process.

## Configuration

There is no further configuration needed for this profile to work correctly.
You will only need to change the configuration to suite your needs.

## Maintainers

Current Maintainers :

* Derek Cresswell - [DerekCresswell](https://www.drupal.org/u/derekcresswell)
* Josh Miller - [joshmiller](https://www.drupal.org/u/joshmiller)

This project is sponsored by :

* [Acro Media Inc.](https://www.acromedia.com/)
    * A leading ecommerce service provider, giving the insights & development online retailers need to optimize their technology for scalable growth and innovation.
